﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;

namespace SpaceBattles
{
    class LoadImage
    {
        static LoadImage()
        {
            GL.Disable(EnableCap.CullFace);
            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.Texture2D);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
            GL.Enable(EnableCap.CullFace);
        }

        public static int GetTextureFromFile(string file)
        {
            Bitmap gdibm = new Bitmap(Bitmap.FromFile(file));
            gdibm.RotateFlip(RotateFlipType.RotateNoneFlipY);
            System.Drawing.Imaging.BitmapData gdibmD = gdibm.LockBits(new Rectangle(0, 0, gdibm.Width, gdibm.Height),
              System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            int itn = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, itn);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, gdibmD.Width, gdibmD.Height,
              0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, gdibmD.Scan0);
            gdibm.UnlockBits(gdibmD);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            return itn;
        }
    }

    public class dWorldRect
    {
        public double X = 0f, Y = 0f, W = 0f, H = 0f;

        public dWorldRect()
        {

        }

        public dWorldRect(double x, double y, double w, double h)
        {
            X = x; Y = y; W = w; H = h;
        }

        public double dTop() { return (Y + H); }
        public double dLeft() { return (X); }
        public double dBottom() { return (Y); }
        public double dRight() { return (X + W); }
    }
}