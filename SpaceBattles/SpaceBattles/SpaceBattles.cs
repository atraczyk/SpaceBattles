﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;

namespace SpaceBattles
{
    public class Game1 : GameWindow
    {
        public enum PlayerNumber { one = 0, two = 1, three = 2, four = 3 };
        public Color4[] PlayerColors = new Color4[4]  {     new Color4(0.5f, 0.5f, 1f, 1f), new Color4(0.5f, 1f, 0.5f, 1f), 
                                                            new Color4(1f, 0.5f, 0.5f, 1f), new Color4(.6f, 0.6f, .6f, 1f)  };
        
        const double inverse_input_framerate = 1f / 60f;
        const double inverse_physics_framerate = 1f / 60f;
        const double inverse_stats_framerate = 1f / 6000f;
        double input_timer = 0f;
        double physics_timer = 0f;
        double stats_timer = 0f;
        double time_factor = 1.0f;
        double zoom = 0.8f;

        Keyboard;

        int background, venus, earth, moon, mars, neptune, particle, ship;

        List<SB_HumanPlayer> HumanPlayers = new List<SB_HumanPlayer>();        
        SB_ParticleManager Projman = new SB_ParticleManager();
        List<SB_MassiveBody> Bodies = new List<SB_MassiveBody>();

        Color4 temp_particle_color = Color4.White;

        public Game1() : base(1024, 1024) { VSync = VSyncMode.Off; }

        protected override void OnLoad(EventArgs e)
        {
            System.Runtime.GCSettings.LatencyMode = System.Runtime.GCLatencyMode.LowLatency;

            GL.ClearColor(Color.Black);

            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

            string startupPath = System.IO.Directory.GetCurrentDirectory();

            background = LoadImage.GetTextureFromFile(  startupPath + "/../Data/Textures/space.png");
            particle = LoadImage.GetTextureFromFile(startupPath + "/../Data/Textures/particle1.png");
            ship = LoadImage.GetTextureFromFile(startupPath + "/../Data/Textures/ship.png");
            venus = LoadImage.GetTextureFromFile(startupPath + "/../Data/Textures/venus.png");
            earth = LoadImage.GetTextureFromFile(startupPath + "/../Data /Textures/earth.png");
            moon = LoadImage.GetTextureFromFile(startupPath + "/../Data/Textures/moon.png");
            mars = LoadImage.GetTextureFromFile(startupPath + "/../Data/Textures/mars.png");
            neptune = LoadImage.GetTextureFromFile(startupPath + "/../Data/Textures/neptune.png");

            HumanPlayers.Add(new SB_HumanPlayer(new Vector2(0f, 0.5f), 0.025f, PlayerColors[0], 0));
            HumanPlayers.Add(new SB_HumanPlayer(new Vector2(0f, -0.5f), 0.025f, PlayerColors[1], 1));
            HumanPlayers.Add(new SB_HumanPlayer(new Vector2(-0.5f, 0f), 0.025f, PlayerColors[2], 2));
            HumanPlayers.Add(new SB_HumanPlayer(new Vector2(0.5f, 0f), 0.025f, PlayerColors[3], 3));

            HumanPlayers[(int)PlayerNumber.one].BindControlKey(Control.left, OpenTK.Input.Key.A);
            HumanPlayers[(int)PlayerNumber.one].BindControlKey(Control.right, OpenTK.Input.Key.D);
            HumanPlayers[(int)PlayerNumber.one].BindControlKey(Control.forward, OpenTK.Input.Key.W);
            HumanPlayers[(int)PlayerNumber.one].BindControlKey(Control.backward, OpenTK.Input.Key.S);
            HumanPlayers[(int)PlayerNumber.one].BindControlKey(Control.powerup, OpenTK.Input.Key.E);
            HumanPlayers[(int)PlayerNumber.one].BindControlKey(Control.powerdown, OpenTK.Input.Key.Q);
            HumanPlayers[(int)PlayerNumber.one].BindControlKey(Control.shoot, OpenTK.Input.Key.Space);

            HumanPlayers[(int)PlayerNumber.two].BindControlKey(Control.left, OpenTK.Input.Key.Keypad1);
            HumanPlayers[(int)PlayerNumber.two].BindControlKey(Control.right, OpenTK.Input.Key.Keypad3);
            HumanPlayers[(int)PlayerNumber.two].BindControlKey(Control.forward, OpenTK.Input.Key.Keypad5);
            HumanPlayers[(int)PlayerNumber.two].BindControlKey(Control.backward, OpenTK.Input.Key.Keypad2);
            HumanPlayers[(int)PlayerNumber.two].BindControlKey(Control.powerup, OpenTK.Input.Key.Keypad6);
            HumanPlayers[(int)PlayerNumber.two].BindControlKey(Control.powerdown, OpenTK.Input.Key.Keypad4);
            HumanPlayers[(int)PlayerNumber.two].BindControlKey(Control.shoot, OpenTK.Input.Key.Keypad0);

            HumanPlayers[(int)PlayerNumber.three].BindControlKey(Control.left, OpenTK.Input.Key.J);
            HumanPlayers[(int)PlayerNumber.three].BindControlKey(Control.right, OpenTK.Input.Key.L);
            HumanPlayers[(int)PlayerNumber.three].BindControlKey(Control.forward, OpenTK.Input.Key.I);
            HumanPlayers[(int)PlayerNumber.three].BindControlKey(Control.backward, OpenTK.Input.Key.K);
            HumanPlayers[(int)PlayerNumber.three].BindControlKey(Control.powerup, OpenTK.Input.Key.O);
            HumanPlayers[(int)PlayerNumber.three].BindControlKey(Control.powerdown, OpenTK.Input.Key.U);
            HumanPlayers[(int)PlayerNumber.three].BindControlKey(Control.shoot, OpenTK.Input.Key.Semicolon);

            HumanPlayers[(int)PlayerNumber.four].BindControlKey(Control.left, OpenTK.Input.Key.Left);
            HumanPlayers[(int)PlayerNumber.four].BindControlKey(Control.right, OpenTK.Input.Key.Right);
            HumanPlayers[(int)PlayerNumber.four].BindControlKey(Control.forward, OpenTK.Input.Key.Up);
            HumanPlayers[(int)PlayerNumber.four].BindControlKey(Control.backward, OpenTK.Input.Key.Down);
            HumanPlayers[(int)PlayerNumber.four].BindControlKey(Control.powerup, OpenTK.Input.Key.Comma);
            HumanPlayers[(int)PlayerNumber.four].BindControlKey(Control.powerdown, OpenTK.Input.Key.Period);
            HumanPlayers[(int)PlayerNumber.four].BindControlKey(Control.shoot, OpenTK.Input.Key.RShift);

            HumanPlayers[(int)PlayerNumber.one].texture = ship;
            HumanPlayers[(int)PlayerNumber.two].texture = ship;
            HumanPlayers[(int)PlayerNumber.three].texture = ship;
            HumanPlayers[(int)PlayerNumber.four].texture = ship;

            Bodies.Add(new SB_MassiveBody(new Vector2(-0.439f, -0.439f), 0.122f, 972000000f, venus,Color4.White,10f));
            Bodies.Add(new SB_MassiveBody(new Vector2(-0.436f, 0.564f), 0.064f, 597000000f, earth, Color4.White,-10f));
            Bodies.Add(new SB_MassiveBody(new Vector2(-0.733f, 0.483f), 0.034f, 1460000f, moon, Color4.White,4f));
            Bodies.Add(new SB_MassiveBody(new Vector2(0.534f, -.091f), 0.034f, 6410000f, mars, Color4.White,-4f));
            Bodies.Add(new SB_MassiveBody(new Vector2(0.246f, 0.246f), 0.246f, 1020000000f, neptune, Color4.White,20f));
            
        }

        protected void UnLoadTexture(int texture)
        {
            GL.DeleteTextures(1, ref texture);
        }

        protected override void OnUnload(EventArgs e)
        {
            UnLoadTexture(background);
            UnLoadTexture(venus);
            UnLoadTexture(earth);
            UnLoadTexture(moon);
            UnLoadTexture(mars);
            UnLoadTexture(neptune);
            UnLoadTexture(particle);
            UnLoadTexture(ship);
        }

        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, Width, Height);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-1.0, 1.0, -1.0, 1.0, 0.0, 4.0);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            input_timer += e.Time;
            if (input_timer >= inverse_input_framerate)
            {
                input_timer = 0f;

                if (Keyboard[OpenTK.Input.Key.Escape])
                    this.Exit();
                if (Keyboard[OpenTK.Input.Key.Minus])
                    zoom += (float)inverse_input_framerate * 1.0f;
                if (Keyboard[OpenTK.Input.Key.Plus])
                    zoom -= (float)inverse_input_framerate * 1.0f;

                if (HumanPlayers[(int)PlayerNumber.one].isalive)
                    HumanPlayers[(int)PlayerNumber.one].ProcessInput(inverse_input_framerate, Keyboard, Projman);
                if (HumanPlayers[(int)PlayerNumber.two].isalive)
                    HumanPlayers[(int)PlayerNumber.two].ProcessInput(inverse_input_framerate, Keyboard, Projman);
                if (HumanPlayers[(int)PlayerNumber.three].isalive)
                    HumanPlayers[(int)PlayerNumber.three].ProcessInput(inverse_input_framerate, Keyboard, Projman);
                if (HumanPlayers[(int)PlayerNumber.four].isalive)
                    HumanPlayers[(int)PlayerNumber.four].ProcessInput(inverse_input_framerate, Keyboard, Projman);
            }

            physics_timer += e.Time;
            if (physics_timer >= inverse_physics_framerate)
            {
                physics_timer = 0f;

                if(HumanPlayers[(int)PlayerNumber.one].isalive)
                    HumanPlayers[(int)PlayerNumber.one].Update(time_factor * inverse_physics_framerate, Vector2.Zero);
                if (HumanPlayers[(int)PlayerNumber.two].isalive)
                    HumanPlayers[(int)PlayerNumber.two].Update(time_factor * inverse_physics_framerate, Vector2.Zero);
                if (HumanPlayers[(int)PlayerNumber.three].isalive)
                    HumanPlayers[(int)PlayerNumber.three].Update(time_factor * inverse_physics_framerate, Vector2.Zero);
                if (HumanPlayers[(int)PlayerNumber.four].isalive)
                    HumanPlayers[(int)PlayerNumber.four].Update(time_factor * inverse_physics_framerate, Vector2.Zero);

                Projman.Update(time_factor * inverse_physics_framerate, Bodies, HumanPlayers);

                foreach (SB_MassiveBody body in Bodies)
                    body.Update(time_factor * inverse_physics_framerate);
            }

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(-zoom, zoom,  -zoom, zoom, 0.0, 4.0);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            stats_timer += e.Time;
            if (stats_timer >= inverse_stats_framerate)
            {
                stats_timer = 0f;
                this.Title = "FPS: " + 1 / e.Time + "    Particles: " + Projman.projectiles.Count;
            }
            
            GL.Clear(ClearBufferMask.ColorBufferBit);

            DrawFullScreenQuad(background);

            foreach (SB_MassiveBody body in Bodies)
                DrawQuad(body.texture, body.w_position, body.w_radius,body.angle, body.color);

            foreach (SB_Particle projectile in Projman.projectiles)
            {
                temp_particle_color.A = (float)(1 - projectile.time / projectile.decayTime);
                DrawQuad(particle, projectile.w_position, projectile.w_radius,0,PlayerColors[projectile.player_id]);
            }

            HumanPlayers[(int)PlayerNumber.one].Draw();
            HumanPlayers[(int)PlayerNumber.two].Draw();
            HumanPlayers[(int)PlayerNumber.three].Draw();
            HumanPlayers[(int)PlayerNumber.four].Draw();

            DrawDebug(e);

            SwapBuffers();
        }

        protected void DrawQuad(int texture, dWorldRect r,float angle,Color4 color)
        {
            GL.Color4(color.R, color.G, color.B, color.A);
            GL.MatrixMode(MatrixMode.Texture);
            GL.LoadIdentity();
            GL.Translate(0.5f, 0.5f, 0.0f);
            GL.Rotate(angle, 0.0f, 0.0f, 1.0f);
            GL.Translate(-0.5f, -0.5f, -0.0f);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            
            GL.BindTexture(TextureTarget.Texture2D, texture);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Filter4Sgis);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);

            GL.Begin(BeginMode.Quads);

            GL.TexCoord2(0.0f, 1.0f); GL.Vertex2(r.dLeft(), r.dBottom());
            GL.TexCoord2(1.0f, 1.0f); GL.Vertex2(r.dRight(), r.dBottom());
            GL.TexCoord2(1.0f, 0.0f); GL.Vertex2(r.dRight(), r.dTop());
            GL.TexCoord2(0.0f, 0.0f); GL.Vertex2(r.dLeft(), r.dTop());

            GL.End();
        }

        protected void DrawQuad(int texture, Vector2 position ,double radius,double angle, Color4 color)
        {
            GL.Color4(color.R, color.G, color.B, color.A);
            GL.MatrixMode(MatrixMode.Texture);
            GL.LoadIdentity();
            GL.Translate(0.5f, 0.5f, 0.0f);
            GL.Rotate(angle, 0.0f, 0.0f, 1.0f);
            GL.Translate(-0.5f, -0.5f, -0.0f);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            GL.BindTexture(TextureTarget.Texture2D, texture);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Filter4Sgis);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);

            GL.Begin(BeginMode.Quads);

            GL.TexCoord2(0.0f, 1.0f); GL.Vertex2(position.X - radius, position.Y - radius);
            GL.TexCoord2(1.0f, 1.0f); GL.Vertex2(position.X + radius, position.Y - radius);
            GL.TexCoord2(1.0f, 0.0f); GL.Vertex2(position.X + radius, position.Y + radius);
            GL.TexCoord2(0.0f, 0.0f); GL.Vertex2(position.X - radius, position.Y + radius);

            GL.End();
        }

        protected void DrawFullScreenQuad(int texture)
        {
            GL.Color4(1f,1f,1f,1f);
            GL.MatrixMode(MatrixMode.Texture);
            GL.LoadIdentity();
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            GL.BindTexture(TextureTarget.Texture2D, texture);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Filter4Sgis);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.MirroredRepeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.MirroredRepeat);

            GL.Begin(BeginMode.Quads);

            double size = 1f;
            double xvalue = 0;
            double yvalue = 0;

            GL.TexCoord2(0.0f + xvalue, 1.0f + yvalue); GL.Vertex2(-size, -size);
            GL.TexCoord2(1.0f + xvalue, 1.0f + yvalue); GL.Vertex2(size, -size);
            GL.TexCoord2(1.0f + xvalue, 0.0f + yvalue); GL.Vertex2(size, size);
            GL.TexCoord2(0.0f + xvalue, 0.0f + yvalue); GL.Vertex2(-size, size);

            GL.End();
        }

        protected void DrawDebug(FrameEventArgs e)
        {
            Console.WriteLine(e.Time);

        }

        [STAThread]
        public static void Main()
        {
            using (Game1 SB_Game = new Game1())
            {
                SB_Game.Title = String.Format("SpaceBattles");
                SB_Game.VSync = OpenTK.VSyncMode.On;
                SB_Game.Run(120, 60);
            }
        }
    }
}
