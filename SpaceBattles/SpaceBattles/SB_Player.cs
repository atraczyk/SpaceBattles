﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace SpaceBattles
{
    public enum Control { left, right, forward, backward, powerup, powerdown, shoot };

    public class SB_PlayerObject : SB_MovingObject
    {
        public bool isalive = true;
        public int player_id = 0;
        public double health = 100f;
        public double angle = 0f;
        public double last_angle = 0f;
        public double power = 0.5f;
        public double last_power = 0f;
        public bool ishuman = false;
        public float move_speed = 4f;
        public Vector2 input_force = Vector2.Zero;
        public Color4 color = Color4.White;
        public int texture = 0;

        public SB_PlayerObject()
        {

        }

        public override void Update(double elapsedTime, Vector2 world_force)
        {
            w_acceleration = world_force + input_force;
            w_velocity += (float)elapsedTime * w_acceleration;
            w_position += (float)elapsedTime * w_velocity;
            ApplyVelocityDamping(0.9f, 0.001f);
        }

    }

    public class SB_HumanPlayer : SB_PlayerObject
    {
        public Key[] Bindlist = new Key[7];

        public SB_HumanPlayer(Vector2 world_position, double world_radius,Color4 col,int pid)
        {
            color = col;
            w_position = world_position;
            w_radius = world_radius;
            ishuman = true;
            player_id = pid;
        }

        public void BindControlKey(Control control, Key key)
        {
            Bindlist[(int)control] = key;
        }

        public Key GetBindKey(Control control)
        {
            return Bindlist[(int)control];
        }

        public void ProcessInput(double deltaTime,KeyboardDevice keyboard,SB_ParticleManager particle_manager)
        {

            input_force = Vector2.Zero;

            if (keyboard[GetBindKey(Control.left)])
                angle += deltaTime * 10.0f;
            if (keyboard[GetBindKey(Control.right)])
                angle -= deltaTime * 10.0f;
            if (keyboard[GetBindKey(Control.forward)])
            {
                input_force.X += move_speed * (float)Math.Cos(angle);
                input_force.Y += move_speed * (float)Math.Sin(angle);
            }
            if (keyboard[GetBindKey(Control.backward)])
            {
                input_force.X -= move_speed * (float)Math.Cos(angle);
                input_force.Y -= move_speed * (float)Math.Sin(angle);
            }

            if (keyboard[GetBindKey(Control.powerup)])
                power += deltaTime * 1.0f;
            if (keyboard[GetBindKey(Control.powerdown)])
                power -= deltaTime * 1.0f;

            if (keyboard[GetBindKey(Control.shoot)])
            {
                Vector2 velocity, position = w_position;
                float particle_radius = 0.025f;
                velocity.X = (float)(power * Math.Cos(angle));
                velocity.Y = (float)(power * Math.Sin(angle));
                position.X += (float)(w_radius * Math.Cos(angle));
                position.Y += (float)(w_radius * Math.Sin(angle));
                particle_manager.AddProjectile(particle_radius, position, velocity, 1, player_id, 30f);
            }
        }

        public void Draw()
        {
            GL.Color4(color.R, color.G, color.B, health/100f);
            GL.MatrixMode(MatrixMode.Texture);
            GL.LoadIdentity();
            GL.Translate(0.5f, 0.5f, 0.0f);
            GL.Rotate(angle * 180f / (float)Math.PI, 0.0f, 0.0f, 1.0f);
            GL.Translate(-0.5f, -0.5f, -0.0f);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();

            GL.BindTexture(TextureTarget.Texture2D, texture);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Filter4Sgis);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);

            GL.Begin(BeginMode.Quads);

            GL.TexCoord2(0.0f, 1.0f); GL.Vertex2(w_position.X - w_radius, w_position.Y - w_radius);
            GL.TexCoord2(1.0f, 1.0f); GL.Vertex2(w_position.X + w_radius, w_position.Y - w_radius);
            GL.TexCoord2(1.0f, 0.0f); GL.Vertex2(w_position.X + w_radius, w_position.Y + w_radius);
            GL.TexCoord2(0.0f, 0.0f); GL.Vertex2(w_position.X - w_radius, w_position.Y + w_radius);

            GL.End();
        }
    }

    public class SB_ComputerPlayer : SB_PlayerObject
    {
        public SB_ComputerPlayer()
        {

        }
    }
}
