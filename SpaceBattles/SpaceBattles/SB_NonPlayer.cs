﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;

namespace SpaceBattles
{
    public class SB_NonPlayerObject : SB_MovingObject
    {
        public SB_NonPlayerObject()
        {

        }
    }

    public class SB_MassiveBody : SB_NonPlayerObject
    {
        public double mass = 10f;
        public double angle = 0f;
        public int texture = 0;
        public Color4 color = Color4.White;
        public double rotation_speed = 1f;

        public SB_MassiveBody(Vector2 position, double radius, double m,int t,Color4 col,double rotationspeed)
        {
            w_position = position;
            w_radius = radius;
            mass = m;
            texture = t;
            color = col;
            rotation_speed = rotationspeed;
        }

        public void Update(double elapsedTime)
        {
            angle += (float)(elapsedTime * rotation_speed);
        }

    }

    public class SB_Item : SB_Object
    {
        public SB_Item()
        {

        }
    }
}
