﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;

namespace SpaceBattles
{
    class LocalVectors
    {
        public static Vector2 worldforce = Vector2.Zero;
        public static Vector2 direction = Vector2.Zero;
        static LocalVectors()
        {

        }
    }

    public class SB_Particle : SB_NonPlayerObject
    {
        public int type;
        public int player_id;
        public double time;
        public double decayTime;
        public double mass = 10f;

        public SB_Particle()
        {
        }

        public SB_Particle(double world_radius, Vector2 position, Vector2 velocity, int projectiletype, int pid, double decaytime)
        {
            time = 0;
            w_radius = world_radius;
            w_position = position;
            w_velocity = velocity;
            type = projectiletype;
            player_id = pid;
            decayTime = decaytime;
        }

        public void Update(double elapsedTime, List<SB_MassiveBody> bodies)
        {
            LocalVectors.direction = Vector2.Zero;
            LocalVectors.worldforce = Vector2.Zero;

            double rs;
            double mag;

            foreach (SB_MassiveBody body in bodies)
            {
                LocalVectors.direction = body.w_position - this.w_position;
                rs = LocalVectors.direction.LengthSquared;
                LocalVectors.direction.Normalize();
                mag = (0.0000000000667 * body.mass * this.mass * 0.1f / rs);
                LocalVectors.worldforce += (float)mag * LocalVectors.direction;
            }

            time += (float)elapsedTime;
            base.Update(elapsedTime, LocalVectors.worldforce);
        }
    }

    public class SB_ParticleManager
    {
        public List<SB_Particle> projectiles = new List<SB_Particle>();
        static Vector2 direction = Vector2.Zero;
        static int i = 0;

        public SB_ParticleManager()
        {
            
        }

        public void Update(double elapsedTime, List<SB_MassiveBody> bodies,List<SB_HumanPlayer> players)
        {
            for( i = 0; i< projectiles.Count; i++)
            {
                projectiles[i].Update(elapsedTime, bodies);
                if (projectiles[i].time > projectiles[i].decayTime)
                    projectiles.Remove(projectiles[i]);
                else
                {
                    direction = Vector2.Zero;
                    foreach (SB_MassiveBody body in bodies)
                    {
                        direction = body.w_position - projectiles[i].w_position;
                        if (direction.Length <= body.w_radius - projectiles[i].w_radius/4)
                        {
                            projectiles.Remove(projectiles[i]);
                            break;
                        }
                    }
                }
            }
            for ( i = 0; i < projectiles.Count; i++)
            {
                direction = Vector2.Zero;
                foreach (SB_PlayerObject player in players)
                {
                    if (player.isalive)
                    {
                        direction = player.w_position - projectiles[i].w_position;
                        if ((direction.Length <= player.w_radius - projectiles[i].w_radius / 4) && (projectiles[i].player_id != player.player_id))
                        {
                            player.health -= projectiles[i].type;
                            player.isalive = player.health > 0 ? true : false;
                            projectiles.Remove(projectiles[i]);
                            break;
                        }
                    }
                }
            }
        }
        
        public void AddProjectile(double world_radius, Vector2 position, Vector2 velocity, int projectiletype, int pid,float decaytime)
        {
            projectiles.Add(new SB_Particle(world_radius, position, velocity, projectiletype, pid, decaytime));
        }        
    }
}
    