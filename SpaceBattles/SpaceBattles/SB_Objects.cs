﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;

namespace SpaceBattles
{
    public class SB_Object
    {
        public Vector2 w_position = Vector2.Zero;
        public double w_radius = 0.0f;

        public SB_Object()
        {

        }

        public SB_Object(Vector2 world_position, double world_radius)
        {
            w_position = world_position;
            w_radius = world_radius;
        }
    }

    public class SB_MovingObject : SB_Object
    {
        public Vector2 w_velocity = Vector2.Zero;
        public Vector2 w_acceleration = Vector2.Zero;

        public SB_MovingObject()
        {

        }

        public virtual void Update(double elapsedTime, Vector2 worldforce)
        {
            w_acceleration = worldforce;
            w_velocity += (float)elapsedTime * w_acceleration;
            w_position += (float)elapsedTime * w_velocity;
        }

        public void ApplyVelocityDamping(float factor, float threshold)
        {
            w_velocity *= factor;
            if (w_velocity.Length < threshold)
                w_velocity = Vector2.Zero;
        }
    }
}
